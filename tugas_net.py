# -*- coding: utf-8 -*-
"""
Created on Thu Feb 23 09:53:26 2017

@author: MSK-1387
"""

import sqlite3 as sql
import pandas


def create_net_file(filename):
    """
    this function use to make *.net file that contains:
        *Vertices n
        1 kata1
        2 kata2
        3 kata3
        ...
        n kataN
        *Edges
        1 2 435
        1 3 435
        1 5 444
        2 3 453
        ...
    the input from this function is *.csv files that contains:
        kata1,kata2,jumlah
        abdul,haris,123
        aku,galau,4566
        ...
        
    """
    
    #make connection with sqlite database
    connect = sql.connect(":memory:")
    cur = connect.cursor()
    
    #make table in database
    try:cur.execute("create table tb_kata_jumlah (kata_1 string,kata_2 string,jumlah int)")
    
    except:
        print "sorry table has ben created"
    
    #insert file in created table
    with open(filename) as tuples:
        for tuple_ in tuples :
            entry = tuple_.split(",")
            cur.execute("insert into tb_kata_jumlah values ( ?,?,? )", (entry[0],entry[1],entry[2]))
            
    
    #create the uniw words from the  data
    list_kata = pandas.read_csv(filename)
    kata_uniq = list(set(list_kata["k1"].tolist()))
    id_uniq = [i+1 for i in range(len(kata_uniq))]
    id_tuple_uniq = map(lambda x,y : (x,y),id_uniq,kata_uniq)
    
    #create table for unique data
    try:cur.execute("create table tb_set_kata (id int,kata string)")
    
    except:
        print "sorry table has ben created"
    
    for i in range(len(kata_uniq)):
        cur.execute("insert into tb_set_kata values ( ?,? )", (id_uniq[i],kata_uniq[i]))
    
    
    #query for geting .net file entry
    query ="""
    CREATE table hasil as
    SELECT 
        b.id AS kata_1, c.id AS kata_2, a.jumlah
    FROM
         tb_kata_jumlah AS a
            INNER JOIN
         tb_set_kata AS b ON (a.kata_1 = b.kata)
            INNER JOIN
         tb_set_kata AS c ON (a.kata_2 = c.kata)
    ORDER BY b.id,c.id;
    
    """
    cur.execute(query)
    c = cur.execute("select * from hasil")
    
    #create file *.net and fill it with the query result
    filename=filename.split(".")
    filename[1]="net"
    filename=".".join(filename)
    with open(filename,'w') as file_:
        file_.write("*Vertices "+str(len(id_tuple_uniq)))
        file_.write("\n")
        for i in range(len(id_tuple_uniq)):
            file_.write(str(id_tuple_uniq[i][0])+" "+str(id_tuple_uniq[i][1]))
            file_.write("\n")
        file_.write("*Edges")
        file_.write("\n")
        for i in c:
            a=c.fetchone()
            if a==None:
                pass
        
            else :
                file_.write(str(a[0])+" "+str(a[1])+" "+str(a[2]))
                file_.write("\n")
    
    #close connection with sqlite database
    connect.close()

def main():
    #generate list of files that will process
    list_file = []
     
    #process files for get the *.net files
    map(create_net_file,list_file)